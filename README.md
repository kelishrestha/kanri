# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
  2.5.1

* System dependencies

* Configuration

* Database creation
  rails db:create

* Database migration
  rails db:migrate

* Database initialization

* How to run the server?
  rails s

  On different port than default

  rails s -p 7878

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* How to create migration file?
  rails g migration AddProfileToUsers profile:string

* How to create model only?
  rails g model Profile age:string dob:datetime address:string

* How to create controller only?
  rails g controller Profiles

* Guidelines
  Rails Commands: https://guides.rubyonrails.org/command_line.html
  Rails Migration: https://guides.rubyonrails.org/active_record_migrations.html
  Rails CRUD operations and conventions: https://guides.rubyonrails.org/active_record_basics.html

